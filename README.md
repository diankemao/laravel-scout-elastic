# Laravel Scout Elasticsearch Driver

This package provides a [Elasticsearch](https://www.elastic.co/products/elasticsearch) driver for Laravel Scout.
Required to use the `Elasticsearch 7.x` or `8.x` Version

## Contents

- [Installation](#installation)
- [Usage](#usage)
- [Credits](#credits)
- [License](#license)

## Installation

You can install the package via composer:

```bash
composer require diankemao/laravel-scout-elastic
```

Laravel will automatically register the driver service provider.

#### Install elasticsearch-php client AND Install laravel scout

For use this library we recomend using the latest version at this time `(^5.5)`

```bash
composer require elasticsearch/elasticsearch:'^5.5'

composer require laravel/scout:'^7.0'
```

### Setting up Elasticsearch configuration

After you've published the Laravel Scout package configuration, you need to set your driver to `elasticsearch` and add its configuration:

```php
// config/scout.php

    // Set your driver to elasticsearch
    'driver' => env('SCOUT_DRIVER', 'elasticsearch'),

    /*
    |--------------------------------------------------------------------------
    | Elasticsearch Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure your Elasticsearch settings.
    |
    */
    'elasticsearch' => [
        'hosts' => [
            env('ELASTICSEARCH_HOST', 'http://localhost:9200'),
            // [
            //     'host'   => env('ELASTICSEARCH_HOST', 'localhost'),
            //     'port'   => env('ELASTICSEARCH_PORT', '9200'),
            //     'scheme' => env('ELASTICSEARCH_SCHEME', 'https'),
            //     'path'   => env('ELASTICSEARCH_PATH', '/elastic'),
            //     'user'   => env('ELASTICSEARCH_USER', 'username'),
            //     'pass'   => env('ELASTICSEARCH_PASS', 'password'),
            // ]
        ],
    ],

// config/logging.php

    // Enabling elasticsearch Logs
    'enable_log_es' => env('ENABLE_LOG_ES', false),


// config/app.php

    'providers' => [
        // Scout elasticsearch Service Providers...
        Laravel\Scout\ScoutServiceProvider::class,
        Diankemao\LaravelScoutElastic\LaravelScoutElasticProvider::class,
    ],

```

For host configuration you can refer to the official [Elasticsearch documentation](https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html)

## Usage

Now you can use Laravel Scout as described in the [Laravel Scout official documentation](https://laravel.com/docs/8.x/scout)

## Credits

- [Allen.Liang](https://github.com/diankemao)

## License

The MIT License (MIT).
