<?php

/**
 * This is Scout Elasticsearch Driver
 *
 * @author   Allen.Liang
 * @date   2023/10/12
 */
namespace Diankemao\LaravelScoutElastic;

use Diankemao\LaravelScoutElastic\Engines\ElasticsearchEngine;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\ServiceProvider;
use Laravel\Scout\EngineManager;
use Monolog\Logger;

class LaravelScoutElasticProvider extends ServiceProvider
{
    private $hosts = '';

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->ensureElasticClientIsInstalled();

        // set log
        $client = ClientBuilder::create()->setHosts($this->hosts)
            ->setConnectionPool('\Elasticsearch\ConnectionPool\SimpleConnectionPool', []);
        $client = $client->setRetries(3);
        if (config('logging.enable_log_es', false)) { // 判断是否开启ES请求日志
            $logFile = storage_path('logs/elastic/') . 'es-' . date('Y-m-d') . '.log';
            $logger = ClientBuilder::defaultLogger($logFile, Logger::DEBUG);
            $client = $client->setLogger($logger);
        }
        resolve(EngineManager::class)->extend('elasticsearch', function () use ($client) {
            return new ElasticsearchEngine(
                $client->build()
            );
        });
    }

    /**
     * Ensure the Elastic API client is installed.
     *
     * @throws \Exception
     */
    protected function ensureElasticClientIsInstalled()
    {
        $this->hosts = config('scout.elasticsearch.hosts', '');
        if (empty($this->hosts)) {
            throw new \Exception('The elasticsearch hosts cannot be empty');
        }

        if (class_exists(ClientBuilder::class)) {
            return;
        }

        throw new \Exception('Please install the Elasticsearch PHP client: elasticsearch/elasticsearch.');
    }
}
