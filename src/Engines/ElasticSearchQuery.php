<?php

/**
 * This is Scout Elasticsearch Driver
 *
 * @author   Allen.Liang
 * @date   2023/10/12
 */
namespace Diankemao\LaravelScoutElastic\Engines;

use Elasticsearch\ClientBuilder;
use Illuminate\Container\Container;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Monolog\Logger;

class ElasticSearchQuery
{
    private $hosts = '';

    private $client;

    private $index;

    private $model;

    private $from = 0;

    private $size = 15;

    private $body = [];

    private $query = [];

    private $aggs = [];

    private $source = [];

    private $orders = [];

    public function __construct($modelClass = null)
    {
        if (! empty($modelClass)) {
            $this->within($modelClass);
        }
        $this->hosts = config('scout.elasticsearch.hosts', '');
        if (empty($this->hosts)) {
            throw new \Exception('The elasticsearch hosts cannot be empty');
        }
        // set log
        $client = ClientBuilder::create()->setHosts($this->hosts)
            ->setConnectionPool('\Elasticsearch\ConnectionPool\SimpleConnectionPool', []);
        $client->setRetries(3);
        if (config('logging.enable_log_es', false)) { // 判断是否开启ES请求日志
            $logFile = storage_path('logs/elastic/') . 'es-' . date('Y-m-d') . '.log';
            $logger = ClientBuilder::defaultLogger($logFile, Logger::DEBUG);
            $client->setLogger($logger);
        }
        $this->client = $client->build();
    }

    /**
     * Specify a custom index to perform this search on.
     * @param $modelClass
     * @return $this
     * @throws \Exception
     */
    public function within($modelClass = null)
    {
        if (empty($modelClass)) {
            throw new \Exception('The model class cannot be empty');
        }
        if (! class_exists($modelClass)) {
            throw new \Exception('The Model Class does not exist');
        }

        $this->model = app($modelClass);
        if (! $index = $this->model->searchableAs()) {
            throw new \Exception('The index cannot be empty');
        }

        $this->index = $index;

        return $this;
    }

    /**
     * Paging for database data.
     * @param null $perPage
     * @param string $page_name
     * @param null $page
     * @return mixed
     */
    public function paginate($perPage = null, $page_name = 'page', $page = null)
    {
        $page = $page ?: Paginator::resolveCurrentPage('page');

        $perPage = $perPage ?: $this->model->getPerPage();

        $this->from = $perPage * ($page - 1);

        $this->size = $perPage;

        $results = $this->getRaw();

        return Container::getInstance()->makeWith(LengthAwarePaginator::class, [
            'items'       => collect($results['hits']['hits'] ?? [])->pluck('_source')->values()->all(),
            'total'       => $this->getTotalCount($results),
            'perPage'     => $perPage,
            'currentPage' => $page,
            'options'     => [
                'path'     => Paginator::resolveCurrentPath(),
                'pageName' => 'page',
            ],
        ]);
    }

    /**
     * Paging for Elasticsearch data.
     * @param null $perPage
     * @param string $page_name
     * @param null $page
     * @return mixed
     */
    public function paginateRaw($perPage = null, $page_name = 'page', $page = null)
    {
        $page = $page ?: Paginator::resolveCurrentPage('page');

        $perPage = $perPage ?: $this->model->getPerPage();

        $this->from = $perPage * ($page - 1);

        $this->size = $perPage;

        $results = $this->getRaw();

        return Container::getInstance()->makeWith(LengthAwarePaginator::class, [
            'items'       => $this->mapIds($results),
            'total'       => $this->getTotalCount($results),
            'perPage'     => $perPage,
            'currentPage' => $page,
            'options'     => [
                'path'     => Paginator::resolveCurrentPath(),
                'pageName' => 'page',
            ],
        ]);
    }

    /**
     * Specify query field array.
     * @return $this
     */
    public function select(array $filed = [])
    {
        $this->source = array_unique(array_filter(array_map('trim', $filed)));

        return $this;
    }

    /**
     * Specify query field string.
     * @return $this
     */
    public function selectRaw(string $filed = '')
    {
        $this->source = array_unique(array_filter(array_map('trim', empty($filed) ? [] : explode(',', $filed))));

        return $this;
    }

    /**
     * Query request.
     * @return array
     */
    public function get()
    {
        $map = [
            'index' => $this->index,
            'type'  => '_doc',
            'body'  => $this->getBody(),
        ];

        $results = $this->client->search($map);
        return collect($results['hits']['hits'] ?? [])->pluck('_source')->values()->all();
    }

    /**
     * Native query request.
     * @return array
     */
    public function getRaw(array $params = [])
    {
        if (! empty($params)) {
            $this->body = $params;
        }
        $map = [
            'index' => $this->index,
            'type'  => '_doc',
            'body'  => $this->getBody(),
        ];

        return $this->client->search($map);
    }

    /**
     * Query condition.
     * @return $this
     */
    public function query(array $query = [])
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Aggregate query.
     * @return $this
     */
    public function aggs(array $aggs = [])
    {
        $this->aggs = $aggs;

        return $this;
    }

    /**
     * Add an "order" for the search query.
     *
     * @param  string  $column
     * @param  string  $direction
     * @return $this
     */
    public function orderBy($column, $direction = 'asc')
    {
        if (empty($column)) {
            return $this;
        }
        if (is_array($direction) && !empty($direction)) {
            $this->orders[$column] = $direction;
        } else {
            $this->orders[$column] = strtolower($direction) == 'asc' ? 'asc' : 'desc';
        }

        return $this;
    }

    /**
     * Set the "limit" for the search query.
     *
     * @param int $limit
     * @return $this
     */
    public function take($limit)
    {
        $this->size = $limit;

        return $this;
    }

    /**
     * Get the first item from the collection.
     * @return array|mixed
     */
    public function first()
    {
        $map = [
            'index' => $this->index,
            'type'  => '_doc',
            'body'  => $this->take(1)->getBody(),
        ];

        $results = $this->client->search($map);
        $results =  collect($results['hits']['hits'] ?? [])->pluck('_source')->values()->all();
        return $results[0] ?? [];
    }

    /**
     * Total number of data items.
     * @param mixed $results
     * @return int
     */
    private function getTotalCount($results)
    {
        return (int) ($results['hits']['total']['value'] ?? 0);
    }

    /**
     * Get database data based on Elasticsearch query results.
     * @return mixed
     */
    private function mapIds($results)
    {
        if ($this->getTotalCount($results) === 0) {
            return $this->model->newCollection();
        }

        $keys = collect($results['hits']['hits'])->pluck('_id')->values()->all();

        $model = new $this->model();

        $list = $this->model->where(function ($query) use ($keys, $model) {
            return $query->whereIn(
                $model->getScoutKeyName(),
                $keys
            );
        })->select($this->source ?: '*')->get();

        return $list;
    }

    /**
     * Get query statement.
     * @return array
     */
    private function getBody()
    {
        $this->body = [
            'from' => (int) $this->from,
            'size' => (int) $this->size,
            'track_total_hits' => true
        ];

        if (! empty($this->query)) {
            $this->body['query'] = $this->query;
        }

        if (! empty($this->source)) {
            $this->body['_source'] = $this->source;
        }

        if (! empty($this->aggs)) {
            $this->body['aggs'] = $this->aggs;
        }

        if (! empty($this->orders)) {
            $this->body['sort'] = $this->orders;
        }

        return $this->body;
    }
}
