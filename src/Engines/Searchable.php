<?php

/**
 * This is Scout Elasticsearch Driver
 *
 * @author   Allen.Liang
 * @date   2023/10/12
 */
namespace Diankemao\LaravelScoutElastic\Engines;

use Diankemao\LaravelScoutElastic\Engines\ElasticSearchQuery;
use Laravel\Scout\Searchable as ScoutSearchable;

trait Searchable
{
    use ScoutSearchable;

    /**
     * The index data of the model enables the execution of native queries。
     * @return array
     */
    public static function searchRaw()
    {
        return new ElasticSearchQuery(self::class);
    }
}
